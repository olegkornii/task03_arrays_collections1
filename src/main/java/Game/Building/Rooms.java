package Game.Building;

import Game.Characters.InRoom;

/**
 * Represent base of rooms
 */
public class Rooms {

    public Room[] roomsArray = new Room[10];

    /**
     * Fill the array
     */
    public void fillRooms(){

        for (int i = 0; i < roomsArray.length ; i++) {
            roomsArray[i] = new Room(i+1);
        }
    }

    /**
     * Show what inside all rooms
     */
    public void openAllRooms(){

        for (int i = 0; i < roomsArray.length ; i++) {
            System.out.println(roomsArray[i]);
        }
    }


    public Room getRoom(int n){
        return roomsArray[n-1];
    }

    /**
     * Check does all rooms are opened
     * @return true if does and false if doesn't
     */
    public boolean allRoomsAreOpened(){

        for (int i = 0; i < roomsArray.length ; i++) {

            if (!roomsArray[i].isOpened()){
                return false;
            }

            if (roomsArray[i].isOpened()
                    && i == roomsArray.length - 1){
                return true;
            }
        }
        return true;
    }
}
