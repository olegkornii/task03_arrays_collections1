package Game.Building;

import Game.Characters.InRoom;
import Game.Characters.Item;
import Game.Characters.Monster;

/**
 * Represent room
 * In room can be monster or item
 */
public class Room {

    private InRoom inRoom;
    private int numberOfRoom;
    private boolean opened;

    public int getNumberOfRoom() {
        return numberOfRoom;
    }

    Room(int numberOfRoom) {

        this.numberOfRoom = numberOfRoom;
        opened = false;
        itemOrMonster();
    }

    /**
     * Generate item or monster in room
     */
    private void itemOrMonster() {

        int itemOrMonster = (int) (Math.random() * 2);
        if (itemOrMonster == 1) {
            inRoom = new Monster();
        } else {
            inRoom = new Item();
        }
    }

    public InRoom getInRoom() {
        return inRoom;
    }

    public boolean isOpened() {
        return opened;
    }

    public void setOpened(boolean wasOpened) {
        this.opened = wasOpened;
    }


    @Override
    public String toString() {
        return "In room #" + numberOfRoom + " is "
                + inRoom
                + "\n______________________";
    }
}
