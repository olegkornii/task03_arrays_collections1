package Game;

import Game.Building.Room;
import Game.Building.Rooms;
import Game.Characters.Hero;
import Game.Characters.Item;
import Game.Characters.Monster;

import java.util.Scanner;

/**
 * Represent game performence
 */
public class Performence {

    private Rooms rooms = new Rooms();
    private Hero hero = new Hero();

    public Performence() {
        rooms.fillRooms();
    }

    /**
     * begin the game
     */
    public void startPerformence() {

        do {

            roomChoise();

        } while (!rooms.allRoomsAreOpened() && hero.isAlive());

    }

    /**
     * Get user input.
     * Open room that was chosen by user
     */
    private void roomChoise() {

        int choise;

        do {

            System.out.println("Chose the room");
            Scanner scanner = new Scanner(System.in);
            choise = scanner.nextInt();
            openRoom(choise);

            if (choise == 0) {
                System.out.println("You quit");
                break;
            }

        } while (choise < 0 || choise > 10);

        rooms.getRoom(choise);
    }

    /**
     * Interaction between user and monster
     *
     * @param room that user in
     */
    private void interaction(Room room) {

        if (room.getInRoom() instanceof Item) {

            hero.setPower(hero.getPower() + ((Item) room.getInRoom()).getPowerIncreace());

        } else if (room.getInRoom() instanceof Monster) {

            if (hero.getPower() >= ((Monster) room.getInRoom()).getPower()) {

                System.out.println("You killed monster!!!!");
            } else {

                System.out.println("You were killed by monster");
                hero.setAlive(false);
            }
        }
    }

    /**
     * Open room that was choosen by user
     *
     * @param numberOfRoom number of room
     */
    private void openRoom(int numberOfRoom) {

        if (rooms.roomsArray[numberOfRoom - 1].isOpened()) {

            System.out.println("You already was in room #" + numberOfRoom);

        } else {

            rooms.roomsArray[numberOfRoom - 1].setOpened(true);
            System.out.println(rooms.roomsArray[numberOfRoom - 1]);
            interaction(rooms.roomsArray[numberOfRoom - 1]);

        }

    }

    /**
     * Calculate number of rooms with monster, that can kill hero, inside
     */
    public void calculateDeathRooms() {

        for (Room room :
                rooms.roomsArray) {

            if (room.getInRoom() instanceof Monster
                    && ((Monster) room.getInRoom()).getPower() > hero.getPower()) {

                System.out.println("In room #" + room.getNumberOfRoom()
                        + " Death are waiting for you");
            }
        }
    }

    /**
     * Auto complete of the game
     */
    public void calculateWin() {

        for (Room room :
                rooms.roomsArray) {

            if (room.getInRoom() instanceof Monster
                    && ((Monster) room.getInRoom()).getPower() <= hero.getPower()
                    && !room.isOpened()
                    || room.getInRoom() instanceof Item
                    && !room.isOpened()) {

                int numberOfRoom = room.getNumberOfRoom();
                openRoom(numberOfRoom);
            }
        }

        if (!rooms.allRoomsAreOpened()) {

            calculateWin();
        } else {

            System.out.println("You WIN");
        }
    }

    /**
     * Set to all rooms field opened == false
     */
    public void setClothedToAll() {

        for (Room room :
                rooms.roomsArray) {
            room.setOpened(false);
        }
    }


}
