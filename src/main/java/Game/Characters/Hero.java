package Game.Characters;

/**
 * Represent Hero
 * Hero has power and can be alive or not
 */
public class Hero implements InRoom {

    private int power = 25;

    private boolean alive = true;

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public boolean isAlive() {
        return alive;

    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }
}
