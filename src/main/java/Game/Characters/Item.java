package Game.Characters;

/**
 * Represent item
 * Item increase power of hero
 */
public class Item implements InRoom {
    int powerIncreace;

    /**
     * Generate random power increase
     */
    public Item() {
        powerIncreace = (int) (Math.random() * 70) + 10;
    }

    public int getPowerIncreace() {
        return powerIncreace;
    }

    @Override
    public String toString() {
        //System.out.println("Item\n Power = "+getPowerIncreace());
        return "Item with Power = " + getPowerIncreace()
                + " you power increase by " + getPowerIncreace();
    }
}
