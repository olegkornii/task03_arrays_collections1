package Game.Characters;

/**
 * Represent monster
 */
public class Monster implements InRoom {

    private int power;

    public Monster() {
        power = (int) (Math.random() * 95) + 5;
    }

    public int getPower() {
        return power;
    }

    @Override
    public String toString() {

        return "monster with" +
                " power=" + power;
    }
}
