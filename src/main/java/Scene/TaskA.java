package Scene;

import Arrays.Arrays;

/**
 * Represent task A
 */
class TaskA {

    private int[] firstArr = new int[10];
    private int[] secondArr = new int[10];
    private Arrays arrays = new Arrays();

    /**
     * Execute task A
     */
    void statTaskA() {

        do {

            arrays.fillTheArray(firstArr);
            arrays.fillTheArray(secondArr);
            System.out.println(java.util.Arrays.toString(firstArr));
            System.out.println(java.util.Arrays.toString(secondArr));
            chooseAction();

        } while (true);
    }

    /**
     * Shows subitems of task A
     */
    private void chooseAction() {

        System.out.println("Show all common values --- Press 1");
        System.out.println("Show all uncommon values --- Press 2");
        System.out.println("Go main menu --- Press 3");

        int choise = UserInput.getUserInput(3);

        switch (choise) {

            case 1: {

                System.out.println(java.util.Arrays.toString(arrays.commonValues(firstArr, secondArr)));
                break;
            }
            case 2: {

                System.out.println(java.util.Arrays.toString(arrays.uncommonValues(firstArr, secondArr)));
                break;
            }
            case 3: {

                Scene.printScene();
            }
        }
    }
}
