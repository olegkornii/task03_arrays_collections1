package Scene;

/**
 * Represent main scene, that user can see
 */
public class Scene {
    /**
     * Print to console main manu
     */
    public static void printScene() {

        System.out.println("A --- Press 1");
        System.out.println("B --- Press 2");
        System.out.println("C --- Press 3");
        System.out.println("D --- Press 4");
        System.out.println("Quit --- Press 0");

        int choice = UserInput.getUserInput(4);

        switch (choice) {
            case 1: {
                TaskA taskA = new TaskA();
                taskA.statTaskA();
                break;
            }
            case 2: {
                TaskB taskB = new TaskB();
                taskB.startTaskB();
                break;
            }
            case 3: {
                TaskC taskC = new TaskC();
                taskC.startTaskC();
                break;
            }
            case 4: {
                TaskD taskD = new TaskD();
                taskD.startTaskD();
                break;

            }
            case 0: {
                System.out.println("You quit from application");
                System.exit(0);
            }
        }
    }
}
