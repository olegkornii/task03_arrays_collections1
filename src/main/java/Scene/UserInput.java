package Scene;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Containce method for user input
 */
public class UserInput {

    public static int getUserInput(int i) {
        int input = -1;
        do {

            try  {
                Scanner scanner = new Scanner(System.in);
                input = scanner.nextInt();
                if( input < 0 || input > i){
                    throw new InputMismatchException();
                }

            }catch (InputMismatchException e){

                System.out.println("You enter incorrect data, please try again");
            }
        }while (input < 0 || input > i);

        return input;
    }
}
