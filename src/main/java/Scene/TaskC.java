package Scene;

import Arrays.Arrays;

/**
 * Represent task C
 */
public class TaskC {
    /**
     * Execute task C
     */
    public void startTaskC() {
        do {

            System.out.println("Delete inline repeats--- Press 1");
            System.out.println("Go main menu --- Press 3*");

            int choice = UserInput.getUserInput(2);

            switch (choice) {

                case 1: {

                    int[] arr = {0, 3, 2, 5, 7, 5, 4, 11, 5, 5, 3, 2, 2, 2, 2, 2, 2, 2, 2, 10, 10};
                    Arrays arrays = new Arrays();
                    System.out.println(java.util.Arrays.toString(arr));
                    System.out.println(java.util.Arrays.toString(arrays.deleteInlineRepeats(arr)));
                    break;
                }

                case 2: {

                    Scene.printScene();
                    break;
                }
            }

        } while (true);
    }
}
