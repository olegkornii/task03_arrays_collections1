package Scene;

import Game.Performence;

import java.util.Scanner;

/**
 * Represent task D
 */
public class TaskD {
    /**
     * Execute  task D
     */
    public void startTaskD() {

        Performence performence = new Performence();
        do {

            chooseOption(performence);

        } while (true);

    }

    /**
     * Show subitems of task D
     *
     * @param performence new game
     */
    private void chooseOption(Performence performence) {

        System.out.println("Begin game --- Press 1*");
        System.out.println("Calculate death rooms --- Press 2*");
        System.out.println("Show one-by-one rooms to enter to win the game");
        System.out.println("Auto complete --- Press 4*");
        System.out.println("Go main menu --- Press 5*");

        int choice = UserInput.getUserInput(5);

        switch (choice) {
            case 1: {

                performence.startPerformence();
                performence.setClothedToAll();
                break;
            }
            case 2: {

                performence.calculateDeathRooms();
                performence.setClothedToAll();
                break;
            }
            case 3: {

                System.out.println("Not ready yet((((");
                performence.setClothedToAll();
                break;
            }
            case 4: {

                performence.calculateWin();
                performence.setClothedToAll();
                break;
            }

            case 5: {
                Scene.printScene();
                break;
            }
        }
    }
}
