package Scene;

import Arrays.Arrays;

/**
 * Represent task B
 */
public class TaskB {
    int[] arr = {1, 1, 1, 2, 3, 4, 5, 2, 3, 2, 11};

    /**
     * Execute task B
     */
    public void startTaskB() {
        do {

            System.out.println("Delete numbers, that repeats more then 2 times --- Press 1*");
            System.out.println("Go main menu --- Press 2*");

            int choice = UserInput.getUserInput(2);

            switch (choice) {
                case 1: {

                    Arrays arrays = new Arrays();
                    System.out.println(java.util.Arrays.toString(arr));
                    System.out.println(java.util.Arrays.toString(arrays.deleteMoreThanTwoRepeats(arr)));
                    break;
                }
                case 2: {

                    Scene.printScene();
                    break;
                }
            }

        } while (true);
    }
}
