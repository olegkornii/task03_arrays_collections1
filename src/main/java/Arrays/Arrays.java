package Arrays;

/**
 * Contains methods for tasks
 */
public class Arrays {
    /**
     * Fills an array with random numbers
     *
     * @param arr that will be filled
     */
    public void fillTheArray(int[] arr) {

        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * 10);
        }
    }

    /**
     * @param fArr first array
     * @param sArr first array
     * @return third array, that contains common values from two arrays
     */
    public int[] commonValues(int[] fArr, int[] sArr) {
        int[] thirdArray = new int[10];
        int z = 0;

        for (int i = 0; i < fArr.length; i++) {
            for (int j = 0; j < sArr.length; j++) {
                if (fArr[i] == sArr[j] && !alreadyExist(thirdArray, fArr[i])) {
                    thirdArray[z++] = fArr[i];

                }
            }
        }

        return thirdArray;
    }


    /**
     * Shows, does value exist in array
     *
     * @param arr array
     * @param v   value
     * @return true if does and false if not
     */
    private boolean alreadyExist(int[] arr, int v) {
        for (int anArr : arr) {
            if (v == anArr) {
                return true;
            }
        }
        return false;
    }

    /**
     * Shows uncommononValues in two arrays
     *
     * @param fArr first array
     * @param sArr second array
     * @return third array that contains uncommon values
     */
    public int[] uncommonValues(int[] fArr, int[] sArr) {
        int[] thirdArray = new int[10];
        int z = 0;

        for (int i = 0; i < fArr.length; i++) {
            boolean uncommonF = true;
            boolean comparedOnlyWithOneF = true;
            boolean uncommonS = true;
            boolean comparedOnlyWithOneS = true;

            for (int j = 0; j < sArr.length; j++) {
                if (fArr[i] == sArr[j]) {
                    uncommonF = false;
                }
                if (fArr[i] != sArr[j] && j != sArr.length - 1) {
                    comparedOnlyWithOneF = false;
                }
                if (fArr[i] != sArr[j]
                        && j == sArr.length - 1
                        && !alreadyExist(thirdArray, fArr[i])
                        && !comparedOnlyWithOneF
                        && uncommonF) {
                    thirdArray[z++] = fArr[i];
                }
                if (fArr[i] != sArr[j]
                        && i == fArr.length - 1
                        && j == fArr.length - 2
                        && !comparedOnlyWithOneF) {
                    thirdArray[z++] = fArr[i];
                }
                // Second Array
                if (sArr[i] == fArr[j]) {
                    uncommonS = false;
                }
                if (sArr[i] != fArr[j] && j != fArr.length - 1) {
                    comparedOnlyWithOneS = false;
                }
                if (sArr[i] != fArr[j]
                        && j == fArr.length - 1
                        && !alreadyExist(thirdArray, sArr[i])
                        && !comparedOnlyWithOneS
                        && uncommonS) {
                    thirdArray[z++] = sArr[i];
                }
                if (sArr[i] != fArr[j]
                        && i == sArr.length - 1
                        && j == sArr.length - 2
                        && !comparedOnlyWithOneS
                        && !alreadyExist(thirdArray, sArr[i])) {
                    thirdArray[z++] = sArr[i];
                }
            }
        }

        return thirdArray;
    }

    /**
     * Delete repeats in array
     *
     * @param arr array
     * @return new array without repeats
     */
    public int[] deleteRepeats(int[] arr) {
        int[] newArr = new int[arr.length];
        int index = 0;
        for (int i = 0; i < arr.length; i++) {
            boolean sole = true;
            for (int j = 0; j < arr.length; j++) {
                if (i == j) {
                    continue;
                }
                if (arr[i] == arr[j]) {
                    sole = false;
                }
                if (arr[i] != arr[j]
                        && j == arr.length - 1
                        && sole) {
                    newArr[index++] = arr[i];
                }
                if (arr[i] != arr[j]
                        && i == arr.length - 1
                        && j == arr.length - 2
                        && sole) {
                    newArr[index++] = arr[i];
                }
            }
        }
        arr = newArr;
        return arr;
    }

    /**
     * Delete values, that repeats more then two times
     *
     * @param arr array
     * @return new array without repeats
     */
    public int[] deleteMoreThanTwoRepeats(int[] arr) {
        int[] newArr = new int[arr.length];
        int index = 0;
        for (int i = 0; i < arr.length; i++) {
            int repeats = 1;
            for (int j = 0; j < arr.length; j++) {
                if (i == j) {
                    continue;
                }
                if (arr[i] == arr[j]) {
                    repeats++;
                    continue;
                }
                if (arr[i] != arr[j]
                        && j == arr.length - 1
                        && repeats <= 2) {
                    newArr[index++] = arr[i];
                }
                if (arr[i] != arr[j]
                        && i == arr.length - 1
                        && j == arr.length - 2
                        && repeats <= 2) {
                    newArr[index++] = arr[i];
                }
            }
        }
        arr = newArr;
        return arr;
    }

    /**
     * Delete inline repeats
     *
     * @param arr array
     * @return new array without inline repeats
     */
    public int[] deleteInlineRepeats(int[] arr) {
        int[] newArr = new int[arr.length];
        int index = 0;
        int nextIndex;
        for (int i = 0; i < arr.length; i++) {

            nextIndex = i + 1;
            if (nextIndex >= arr.length) {
                newArr[index] = arr[i];
                break;
            }
            if (arr[i] != arr[nextIndex]) {
                newArr[index++] = arr[i];
                i = nextIndex - 1;
            }
            if (arr[i] == arr[nextIndex]) {
                newArr[index++] = arr[i];
                while (arr[i] == arr[nextIndex]) {
                    nextIndex++;
                    if (nextIndex >= arr.length) {
                        break;
                    }
                }
                i = nextIndex - 1;
            }

        }
        arr = newArr;
        return arr;
    }
}
